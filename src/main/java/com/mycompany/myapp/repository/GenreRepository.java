package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer>{

}
