package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.Track;

@Repository
public interface TrackRepository extends JpaRepository<Track, Integer> {

}
