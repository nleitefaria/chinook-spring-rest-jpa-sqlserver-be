package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.Artist;

@Repository
public interface ArtistRepository  extends JpaRepository<Artist, Integer>{

}
