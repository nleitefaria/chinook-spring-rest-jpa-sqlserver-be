package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.Album;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Integer>{

}
