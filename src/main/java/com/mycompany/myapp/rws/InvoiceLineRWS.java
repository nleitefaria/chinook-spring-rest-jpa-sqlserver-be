package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.InvoiceLineDTO;
import com.mycompany.myapp.service.InvoiceLineService;

@RestController
public class InvoiceLineRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(InvoiceLineRWS.class);
	
	@Autowired
	InvoiceLineService invoiceLineService; 
	
	@RequestMapping(value = "/invoiceline/{id}", method = RequestMethod.GET)
	public ResponseEntity<InvoiceLineDTO> findOne(@PathVariable Integer id) {
		logger.info("Listing invoice line with id: " + id);
		return new ResponseEntity<InvoiceLineDTO>(invoiceLineService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/invoicelines", method = RequestMethod.GET)
	public ResponseEntity<List<InvoiceLineDTO>> findAll() {
		logger.info("Listing all invoice lines");
		return new ResponseEntity<List<InvoiceLineDTO>>(invoiceLineService.findAll(), HttpStatus.OK);
	}

}
