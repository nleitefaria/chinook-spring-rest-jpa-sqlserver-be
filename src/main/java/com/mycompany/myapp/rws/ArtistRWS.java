package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.ArtistDTO;
import com.mycompany.myapp.service.ArtistService;

@RestController
public class ArtistRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(ArtistRWS.class);
	
	@Autowired
	ArtistService artistService; 
	
	@RequestMapping(value = "/artist/{id}", method = RequestMethod.GET)
	public ResponseEntity<ArtistDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing artist with id: " + id);
		return new ResponseEntity<ArtistDTO>(artistService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/artists", method = RequestMethod.GET)
	public ResponseEntity<List<ArtistDTO>> findAll() {
		logger.info("Listing all artists");
		return new ResponseEntity<List<ArtistDTO>>(artistService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/artist", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody ArtistDTO artistDTO)
	{       
        logger.info("Creating entity"); 	
		try
		{
			artistService.save(artistDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity: " + e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }


}
