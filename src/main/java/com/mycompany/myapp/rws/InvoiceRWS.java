package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.InvoiceDTO;
import com.mycompany.myapp.service.InvoiceService;

@RestController
public class InvoiceRWS
{
	
	private static final Logger logger = LoggerFactory.getLogger(InvoiceRWS.class);
	
	@Autowired
	InvoiceService invoiceService; 
	
	@RequestMapping(value = "/invoice/{id}", method = RequestMethod.GET)
	public ResponseEntity<InvoiceDTO> findOne(@PathVariable Integer id) {
		logger.info("Listing invoice with id: " + id);
		return new ResponseEntity<InvoiceDTO>(invoiceService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/invoices", method = RequestMethod.GET)
	public ResponseEntity<List<InvoiceDTO>> findAll() {
		logger.info("Listing all genres");
		return new ResponseEntity<List<InvoiceDTO>>(invoiceService.findAll(), HttpStatus.OK);
	}

}
