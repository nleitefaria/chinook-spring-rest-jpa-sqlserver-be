package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.AlbumDTO;
import com.mycompany.myapp.service.AlbumService;

@RestController
public class AlbumRWS
{
	private static final Logger logger = LoggerFactory.getLogger(AlbumRWS.class);
	
	@Autowired
	AlbumService albumService; 
	
	@RequestMapping(value = "/album/{id}", method = RequestMethod.GET)
	public ResponseEntity<AlbumDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing album with id: " + id);
		return new ResponseEntity<AlbumDTO>(albumService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/albuns", method = RequestMethod.GET)
	public ResponseEntity<List<AlbumDTO>> findAll() {
		logger.info("Listing all albuns");
		return new ResponseEntity<List<AlbumDTO>>(albumService.findAll(), HttpStatus.OK);
	}


}
