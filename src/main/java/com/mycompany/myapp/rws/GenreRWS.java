package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.GenreDTO;
import com.mycompany.myapp.service.GenreService;

@RestController
public class GenreRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(GenreRWS.class);
	
	@Autowired
	GenreService genreService; 
	
	@RequestMapping(value = "/genre/{id}", method = RequestMethod.GET)
	public ResponseEntity<GenreDTO> findOne(@PathVariable Integer id) {
		logger.info("Listing genre with id: " + id);
		return new ResponseEntity<GenreDTO>(genreService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/genres", method = RequestMethod.GET)
	public ResponseEntity<List<GenreDTO>> findAll() {
		logger.info("Listing all genres");
		return new ResponseEntity<List<GenreDTO>>(genreService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/genre", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody GenreDTO genreDTO)
	{       
        logger.info("Creating entity"); 	
		try
		{
			genreService.save(genreDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity: " + e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }

}
