package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.PlaylistDTO;
import com.mycompany.myapp.service.PlaylistService;

@RestController
public class PlaylistRWS
{	
	private static final Logger logger = LoggerFactory.getLogger(PlaylistRWS.class);
	
	@Autowired
	PlaylistService playlistService; 
	
	@RequestMapping(value = "/playlist/{id}", method = RequestMethod.GET)
	public ResponseEntity<PlaylistDTO> findOne(@PathVariable Integer id) {
		logger.info("Listing playlist with id: " + id);
		return new ResponseEntity<PlaylistDTO>(playlistService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/playlists", method = RequestMethod.GET)
	public ResponseEntity<List<PlaylistDTO>> findAll() {
		logger.info("Listing all palylists");
		return new ResponseEntity<List<PlaylistDTO>>(playlistService.findAll(), HttpStatus.OK);
	}


}
