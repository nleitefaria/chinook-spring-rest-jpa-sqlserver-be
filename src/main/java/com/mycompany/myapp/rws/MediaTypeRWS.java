package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.MediaTypeDTO;
import com.mycompany.myapp.service.MediaTypeService;

@RestController
public class MediaTypeRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(MediaTypeRWS.class);
	
	@Autowired
	MediaTypeService mediaTypeService; 
	
	@RequestMapping(value = "/mediatype/{id}", method = RequestMethod.GET)
	public ResponseEntity<MediaTypeDTO> findOne(@PathVariable Integer id) {
		logger.info("Listing mediatype with id: " + id);
		return new ResponseEntity<MediaTypeDTO>(mediaTypeService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mediatypes", method = RequestMethod.GET)
	public ResponseEntity<List<MediaTypeDTO>> findAll() {
		logger.info("Listing all mediatypes");
		return new ResponseEntity<List<MediaTypeDTO>>(mediaTypeService.findAll(), HttpStatus.OK);
	}


}
