package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.AlbumDTO;


public interface AlbumService 
{
	long count();
	AlbumDTO findOne(Integer id);
	List<AlbumDTO> findAll();

}
