package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.InvoiceLineDTO;

public interface InvoiceLineService {
	
	long count();
	InvoiceLineDTO findOne(Integer id);
	List<InvoiceLineDTO> findAll();

}
