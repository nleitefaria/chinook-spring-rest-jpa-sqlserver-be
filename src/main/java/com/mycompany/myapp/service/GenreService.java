package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.GenreDTO;

public interface GenreService
{	
	long count();
	GenreDTO findOne(Integer id);
	List<GenreDTO> findAll();
	void save(GenreDTO genreDTO);

}
