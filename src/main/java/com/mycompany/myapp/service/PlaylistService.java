package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PlaylistDTO;

public interface PlaylistService {
	
	long count();
	PlaylistDTO findOne(Integer id);
	List<PlaylistDTO> findAll();

}
