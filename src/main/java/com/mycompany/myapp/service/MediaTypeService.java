package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.MediaTypeDTO;

public interface MediaTypeService
{
	long count();
	MediaTypeDTO findOne(Integer id);
	List<MediaTypeDTO> findAll();
	
}
