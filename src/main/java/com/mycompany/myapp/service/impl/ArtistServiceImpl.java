package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.ArtistDTO;
import com.mycompany.myapp.entity.Artist;
import com.mycompany.myapp.repository.ArtistRepository;
import com.mycompany.myapp.service.ArtistService;

@Service
public class ArtistServiceImpl implements ArtistService
{
	@Autowired
	ArtistRepository artistRepository;

	@Transactional
	public long count() 
	{		
		return artistRepository.count();		
	}
	
	@Transactional
	public ArtistDTO findOne(Integer id) 
	{		
		Artist artist = artistRepository.findOne(id);	
		return new ArtistDTO(artist.getArtistId(), artist.getName());	
	}
	
	@Transactional
	public List<ArtistDTO> findAll() 
	{
		List<ArtistDTO> ret = new ArrayList<ArtistDTO>();
		for(Artist artist : artistRepository.findAll())
		{
			ret.add(new ArtistDTO(artist.getArtistId(), artist.getName()));		
		}		
		return ret;		
	}
	
	@Transactional
	public void save(ArtistDTO artistDTO) 
	{	
		Artist artist = new Artist(artistDTO.getName());
		artistRepository.save(artist);		
	}

}
