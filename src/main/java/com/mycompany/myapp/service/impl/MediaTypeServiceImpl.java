package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.MediaTypeDTO;
import com.mycompany.myapp.entity.MediaType;
import com.mycompany.myapp.repository.MediaTypeRepository;
import com.mycompany.myapp.service.MediaTypeService;

@Service
public class MediaTypeServiceImpl implements MediaTypeService
{
	@Autowired
	MediaTypeRepository mediaTypeRepository;

	@Transactional
	public long count() 
	{		
		return mediaTypeRepository.count();		
	}
	
	@Transactional
	public MediaTypeDTO findOne(Integer id) 
	{		
		MediaType mediaType = mediaTypeRepository.findOne(id);	
		return new MediaTypeDTO(mediaType.getMediaTypeId(), mediaType.getName());	
	}
	
	@Transactional
	public List<MediaTypeDTO> findAll() 
	{
		List<MediaTypeDTO> ret = new ArrayList<MediaTypeDTO>();
		for(MediaType mediaType : mediaTypeRepository.findAll())
		{
			ret.add(new MediaTypeDTO(mediaType.getMediaTypeId(), mediaType.getName()));		
		}		
		return ret;		
	}

}
