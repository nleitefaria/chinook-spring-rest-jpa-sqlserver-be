package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AlbumDTO;
import com.mycompany.myapp.domain.CustomerDTO;
import com.mycompany.myapp.entity.Album;
import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.repository.AlbumRepository;
import com.mycompany.myapp.repository.CustomerRepository;
import com.mycompany.myapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	CustomerRepository customerRepository;

	@Transactional
	public long count() 
	{		
		return customerRepository.count();		
	}
	
	@Transactional
	public CustomerDTO findOne(Integer id) 
	{		
		Customer customer = customerRepository.findOne(id);	
		return new CustomerDTO(customer.getCustomerId(), customer.getFirstName(), customer.getLastName(), customer.getCompany(), customer.getAddress(), customer.getCity(), customer.getState(), customer.getCountry(), customer.getPostalCode(), customer.getPhone(), customer.getFax(), customer.getEmail());
	}
	
	@Transactional
	public List<CustomerDTO> findAll() 
	{
		List<CustomerDTO> ret = new ArrayList<CustomerDTO>();
		for(Customer customer : customerRepository.findAll())
		{
			ret.add(new CustomerDTO(customer.getCustomerId(), customer.getFirstName(), customer.getLastName(), customer.getCompany(), customer.getAddress(), customer.getCity(), customer.getState(), customer.getCountry(), customer.getPostalCode(), customer.getPhone(), customer.getFax(), customer.getEmail()));		
		}		
		return ret;		
	}


}
