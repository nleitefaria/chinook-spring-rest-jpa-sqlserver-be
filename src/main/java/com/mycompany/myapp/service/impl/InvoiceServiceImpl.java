package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.InvoiceDTO;
import com.mycompany.myapp.entity.Invoice;
import com.mycompany.myapp.repository.InvoiceRepository;
import com.mycompany.myapp.service.InvoiceService;

@Service
public class InvoiceServiceImpl implements InvoiceService
{
	@Autowired
	InvoiceRepository invoiceRepository;

	@Transactional
	public long count() 
	{		
		return invoiceRepository.count();		
	}
	
	@Transactional
	public InvoiceDTO findOne(Integer id) 
	{		
		Invoice invoice = invoiceRepository.findOne(id);	
		return new InvoiceDTO(invoice.getInvoiceId(), invoice.getInvoiceDate(), invoice.getBillingAddress(), invoice.getBillingCity(), invoice.getBillingState(), invoice.getBillingCountry(), invoice.getBillingPostalCode(), invoice.getTotal());
	}
	
	@Transactional
	public List<InvoiceDTO> findAll() 
	{
		List<InvoiceDTO> ret = new ArrayList<InvoiceDTO>();
		for(Invoice invoice : invoiceRepository.findAll())
		{
			ret.add(new InvoiceDTO(invoice.getInvoiceId(), invoice.getInvoiceDate(), invoice.getBillingAddress(), invoice.getBillingCity(), invoice.getBillingState(), invoice.getBillingCountry(), invoice.getBillingPostalCode(), invoice.getTotal()));		
		}		
		return ret;		
	}

	
	
	
}
