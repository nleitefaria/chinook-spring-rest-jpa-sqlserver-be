package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.EmployeeDTO;
import com.mycompany.myapp.entity.Employee;
import com.mycompany.myapp.repository.EmployeeRepository;
import com.mycompany.myapp.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeRepository employeeRepository;

	@Transactional
	public long count() 
	{		
		return employeeRepository.count();		
	}
	
	@Transactional
	public EmployeeDTO findOne(Integer id) 
	{		
		Employee employee = employeeRepository.findOne(id);	
		return new EmployeeDTO(employee.getEmployeeId(), employee.getLastName(), employee.getFirstName(), employee.getTitle(), employee.getBirthDate(), employee.getHireDate(), employee.getAddress(), employee.getCity(), employee.getState(), employee.getCountry(), employee.getPostalCode(), employee.getPhone(), employee.getFax(), employee.getEmail());
	}
	
	@Transactional
	public List<EmployeeDTO> findAll() 
	{
		List<EmployeeDTO> ret = new ArrayList<EmployeeDTO>();
		for(Employee employee : employeeRepository.findAll())
		{
			ret.add(new EmployeeDTO(employee.getEmployeeId(), employee.getLastName(), employee.getFirstName(), employee.getTitle(), employee.getBirthDate(), employee.getHireDate(), employee.getAddress(), employee.getCity(), employee.getState(), employee.getCountry(), employee.getPostalCode(), employee.getPhone(), employee.getFax(), employee.getEmail()));		
		}		
		return ret;		
	}

}
