package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.PlaylistDTO;
import com.mycompany.myapp.entity.Playlist;
import com.mycompany.myapp.repository.PlaylistRepository;
import com.mycompany.myapp.service.PlaylistService;

@Service
public class PlaylistServiceImpl implements PlaylistService{
	
	@Autowired
	PlaylistRepository playlistRepository;

	@Transactional
	public long count() 
	{		
		return playlistRepository.count();		
	}
	
	@Transactional
	public PlaylistDTO findOne(Integer id) 
	{		
		Playlist playlist = playlistRepository.findOne(id);	
		return new PlaylistDTO(playlist.getPlaylistId(), playlist.getName());	
	}
	
	@Transactional
	public List<PlaylistDTO> findAll() 
	{
		List<PlaylistDTO> ret = new ArrayList<PlaylistDTO>();
		for(Playlist playlist : playlistRepository.findAll())
		{
			ret.add(new PlaylistDTO(playlist.getPlaylistId(), playlist.getName()));		
		}		
		return ret;		
	}


}
