package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AlbumDTO;
import com.mycompany.myapp.entity.Album;
import com.mycompany.myapp.repository.AlbumRepository;
import com.mycompany.myapp.service.AlbumService;

@Service
public class AlbumServiceImpl implements AlbumService
{
	@Autowired
	AlbumRepository albumRepository;

	@Transactional
	public long count() 
	{		
		return albumRepository.count();		
	}
	
	@Transactional
	public AlbumDTO findOne(Integer id) 
	{		
		Album album = albumRepository.findOne(id);	
		return new AlbumDTO(album.getAlbumId(), album.getTitle());	
	}
	
	@Transactional
	public List<AlbumDTO> findAll() 
	{
		List<AlbumDTO> ret = new ArrayList<AlbumDTO>();
		for(Album album : albumRepository.findAll())
		{
			ret.add(new AlbumDTO(album.getAlbumId(), album.getTitle()));		
		}		
		return ret;		
	}


}
