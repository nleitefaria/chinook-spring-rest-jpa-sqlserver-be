package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.InvoiceLineDTO;
import com.mycompany.myapp.entity.InvoiceLine;
import com.mycompany.myapp.repository.InvoiceLineRepository;
import com.mycompany.myapp.service.InvoiceLineService;

@Service
public class InvoiceLineServiceImpl implements InvoiceLineService {
	
	@Autowired
	InvoiceLineRepository invoiceLineRepository;

	@Transactional
	public long count() 
	{		
		return invoiceLineRepository.count();		
	}
	
	@Transactional
	public InvoiceLineDTO findOne(Integer id) 
	{		
		InvoiceLine invoiceLine = invoiceLineRepository.findOne(id);	
		return new InvoiceLineDTO(invoiceLine.getInvoiceLineId(), invoiceLine.getUnitPrice(), invoiceLine.getQuantity());
	}
	
	@Transactional
	public List<InvoiceLineDTO> findAll() 
	{
		List<InvoiceLineDTO> ret = new ArrayList<InvoiceLineDTO>();
		for(InvoiceLine invoiceLine : invoiceLineRepository.findAll())
		{
			ret.add(new InvoiceLineDTO(invoiceLine.getInvoiceLineId(), invoiceLine.getUnitPrice(), invoiceLine.getQuantity()));		
		}		
		return ret;		
	}

}
