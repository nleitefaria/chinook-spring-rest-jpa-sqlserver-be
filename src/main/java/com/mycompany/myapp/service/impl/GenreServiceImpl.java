package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.GenreDTO;
import com.mycompany.myapp.entity.Genre;
import com.mycompany.myapp.repository.GenreRepository;
import com.mycompany.myapp.service.GenreService;

@Service
public class GenreServiceImpl implements GenreService
{
	@Autowired
	GenreRepository genreRepository;

	@Transactional
	public long count() 
	{		
		return genreRepository.count();		
	}
	
	@Transactional
	public GenreDTO findOne(Integer id) 
	{		
		Genre genre = genreRepository.findOne(id);	
		return new GenreDTO(genre.getGenreId(), genre.getName());	
	}
	
	@Transactional
	public List<GenreDTO> findAll() 
	{
		List<GenreDTO> ret = new ArrayList<GenreDTO>();
		for(Genre genre : genreRepository.findAll())
		{
			ret.add(new GenreDTO(genre.getGenreId(), genre.getName()));		
		}		
		return ret;		
	}
	
	@Transactional
	public void save(GenreDTO genreDTO) 
	{	
		Genre genre = new Genre(genreDTO.getName());
		genreRepository.save(genre);		
	}

}
