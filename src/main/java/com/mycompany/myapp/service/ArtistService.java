package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ArtistDTO;

public interface ArtistService
{	
	long count();
	ArtistDTO findOne(Integer id);
	List<ArtistDTO> findAll();
	void save(ArtistDTO artistDTO);
}
