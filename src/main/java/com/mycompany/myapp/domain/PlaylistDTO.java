package com.mycompany.myapp.domain;

public class PlaylistDTO
{
	private Integer playlistId;
    private String name;
    
	public PlaylistDTO() 
	{
	}

	public PlaylistDTO(Integer playlistId, String name)
	{		
		this.playlistId = playlistId;
		this.name = name;
	}

	public Integer getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(Integer playlistId) {
		this.playlistId = playlistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
