package com.mycompany.myapp.domain;

public class MediaTypeDTO 
{
	private Integer mediaTypeId;
	private String name;
		 
	public MediaTypeDTO() 
	{		
	}

	public MediaTypeDTO(Integer mediaTypeId, String name) {
		this.mediaTypeId = mediaTypeId;
		this.name = name;
	}

	public Integer getMediaTypeId() {
		return mediaTypeId;
	}

	public void setMediaTypeId(Integer mediaTypeId) {
		this.mediaTypeId = mediaTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
