package com.mycompany.myapp.domain;

public class GenreDTO 
{	
	private Integer genreId;
	private String name;
	
	public GenreDTO() 
	{				
	}
	
	public GenreDTO(Integer genreId, String name) 
	{		
		this.genreId = genreId;
		this.name = name;
	}
	
	public Integer getGenreId() {
		return genreId;
	}
	
	public void setGenreId(Integer genreId) {
		this.genreId = genreId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
