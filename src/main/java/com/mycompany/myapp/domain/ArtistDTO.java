package com.mycompany.myapp.domain;


public class ArtistDTO 
{
	private Integer artistId;  
    private String name;
    
    public ArtistDTO() 
	{	
	}
    
	public ArtistDTO(Integer artistId, String name) 
	{	
		this.artistId = artistId;
		this.name = name;
	}

	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
    
    

}
