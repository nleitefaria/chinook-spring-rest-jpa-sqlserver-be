package com.mycompany.myapp.domain;

public class AlbumDTO 
{
	private Integer albumId;
    private String title;
    
    public AlbumDTO()
    {
    }
    
    public AlbumDTO(Integer albumId, String title) 
    {
        this.albumId = albumId;
        this.title = title;
    }
    
	public Integer getAlbumId()
	{
		return albumId;
	}
	
	public void setAlbumId(Integer albumId)
	{
		this.albumId = albumId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}
}
