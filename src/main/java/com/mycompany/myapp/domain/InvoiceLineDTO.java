package com.mycompany.myapp.domain;

import java.math.BigDecimal;

public class InvoiceLineDTO 
{	
	private Integer invoiceLineId;
    private BigDecimal unitPrice;
    private int quantity;
      
	public InvoiceLineDTO() {
		super();
	}

	public InvoiceLineDTO(Integer invoiceLineId, BigDecimal unitPrice, int quantity) {		
		this.invoiceLineId = invoiceLineId;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}

	public Integer getInvoiceLineId() {
		return invoiceLineId;
	}

	public void setInvoiceLineId(Integer invoiceLineId) {
		this.invoiceLineId = invoiceLineId;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
