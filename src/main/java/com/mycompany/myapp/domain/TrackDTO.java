package com.mycompany.myapp.domain;

import java.math.BigDecimal;

public class TrackDTO 
{	
	private Integer trackId;
    private String name;
    private String composer;
    private int milliseconds;  
    private Integer bytes;
    private BigDecimal unitPrice;
    
    public TrackDTO()
    {
    }

    public TrackDTO(Integer trackId, String name, int milliseconds, BigDecimal unitPrice)
    {
        this.trackId = trackId;
        this.name = name;
        this.milliseconds = milliseconds;
        this.unitPrice = unitPrice;
    }

	public Integer getTrackId() {
		return trackId;
	}

	public void setTrackId(Integer trackId) {
		this.trackId = trackId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public int getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}

	public Integer getBytes() {
		return bytes;
	}

	public void setBytes(Integer bytes) {
		this.bytes = bytes;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
}
